import datetime
from uuid import uuid4
import logging
from flask import jsonify, Flask, request
from send_request import SendRequest

from flask_restplus import Api, Resource, fields
from flask_cors import CORS
from blockchain import Blockchain

logging.basicConfig(level=logging.DEBUG, format='[%(levelname)s] (%(threadName)s) %(message)s',)

# Instantiate our Node
app = Flask(__name__)
app.config['CORS_HEADERS'] = 'Content-Type'

CORS(app, resources={
    r'/*': {
        'origins': '*'
    }
})

api = Api(app=app, version='0.1', title='Payment Certification Api', description='Blockchain Api ', validate=True)
ns_blockchain = api.namespace('blockchain', description="Blockchain Certification operations")

transactions = api.model('Transactions', {
    'recipient': fields.String(required=True),
    'amount': fields.Integer(required=True),
    'sender': fields.String(required=True),
    'externalId': fields.String(required=True),
    'partyId': fields.String(required=True)
})
# Generate a globally unique address for this node
node_identifier = str(uuid4()).replace('-', '')

# Instantiate the Blockchain
new_blockchain = Blockchain()

# Instantiate the Request
request_sender = SendRequest()

# GLOBAL VARS
SENDER = "domaineweb.xyz"
RECIPIENT = "https://sandbox.momodeveloper.mtn.com/collection/v1_0/requesttopay"
'''
Methode de lecture dun bloc
'''


@ns_blockchain.route("/block")
class BlockchainApiRead(Resource):
    @staticmethod
    def get():
        # We run the proof of work algorithm to get the next proof...
        last_block = new_blockchain.last_block
        last_proof = last_block['proof']
        proof = new_blockchain.proof_of_work(last_proof)

        # We must receive a reward for finding the proof.
        # The sender is "0" to signify that this node has mined a new coin.
        new_blockchain.add_transaction(
            sender="0",
            recipient=node_identifier,
            amount=1,
        )

        # Forge the new Block by adding it to the chain
        previous_hash = new_blockchain.hash(last_block)
        block = new_blockchain.create_block(proof, previous_hash)

        response = {
            'message': "New Block Forged",
            'index': block['index'],
            'transactions': block['transactions'],
            'proof': block['proof'],
            'previous_hash': block['previous_hash'],
        }
        return jsonify(response)


@ns_blockchain.route("/transactions")
class BlockchainApiNew(Resource):
    @staticmethod
    @api.expect(transactions)
    def post():
        data = request.get_json()
        required = ['sender', 'recipient', 'amount']
        if not all(k in data for k in required):
            return 'Missing values'
        # Create a new Transaction
        index = new_blockchain.add_transaction(data['sender'], data['recipient'], data['amount'])
        response = {'message': 'Transaction will be added to Block {index}'}
        return response


@ns_blockchain.route("/chain")
class BlockchainApi(Resource):
    @staticmethod
    def get():
        response = {
            'chain': new_blockchain.chain,
            'length': len(new_blockchain.chain),
        }
        return jsonify(response)


@ns_blockchain.route('/all_transaction')
class BlockchainAllTransaction(Resource):
    @staticmethod
    @api.expect(transactions)
    def post():
        data = request.get_json()
        response = new_blockchain.all_transactions(SENDER, RECIPIENT, data['amount'], data["externalId"], data["partyId"])
        return jsonify(response)


@ns_blockchain.route('/certification')
class BlockchainCertification(Resource):
    @staticmethod
    @api.expect(transactions)
    def post():
        data = request.get_json()
        status = request_sender.send_request(data["amount"], data["externalId"], data["partyId"])

        # if status in [200, 201, 202, 203]:
        if status:
            last_block = new_blockchain.last_block
            last_proof = last_block['proof']
            proof = new_blockchain.proof_of_work(last_proof)

            new_index = new_blockchain.add_transaction(SENDER, RECIPIENT, data['amount'])

            previous_hash = new_blockchain.hash(last_block)
            block = new_blockchain.create_block(proof, previous_hash)

            response = {
                'code': 0,
                'message': "Paiement certifié le " + str(datetime.datetime.now()) + " pour le " + data["partyId"],
                'index': new_index,
                'transactions': block['transactions'],
                'proof': block['proof'],
                'previous_hash': block['previous_hash'],
            }
        else:
            response = {
                'code': -1,
                'message': 'Error payment request'
            }
        return jsonify(response)


if __name__ == '__main__':
    app.run(host='localhost', port=5000, debug=True)
