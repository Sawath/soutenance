# import list
import uuid
import requests
import base64


class SendRequest:
    def __init__(self):
        self.api_key = None
        self.token = None
        self.x_reference_id = uuid.uuid4()  # UUID V4 GENERATOR
        self.subscription_key = 'bee5e0997ebd4f8c94946f647cf8f357'

    def get_api_key(self):

        # apiuser - POST
        # Used to create an API user in the sandbox target environment

        url = "https://sandbox.momodeveloper.mtn.com/v1_0/apiuser"

        headers = {
            'Content-Type': 'application/json',
            'Ocp-Apim-Subscription-Key': '{subscription_key}'.format(subscription_key=self.subscription_key),
            'X-Reference-Id': '{x_reference_id}'.format(x_reference_id=self.x_reference_id)
        }

        payload = "{\n  \"providerCallbackHost\": \"https://callback-will-not-work-in-the-sandbox.com\"\n}"

        apiuser = requests.post(url, headers=headers, data=payload)
        # apiuser.status_code

        # apiuser/{X-Reference-Id}/apikey - POST
        # Used to create an API key for an API user in the sandbox target environment.

        url = "https://sandbox.momodeveloper.mtn.com/v1_0/apiuser/{x_reference_id}/apikey".format(
            x_reference_id=self.x_reference_id)

        headers = {
            'Ocp-Apim-Subscription-Key': '{subscription_key}'.format(subscription_key=self.subscription_key)
        }

        payload = "{}"

        apikey = requests.post(url, headers=headers, data=payload)
        # apikey.status_code
        apikey = apikey.json()
        apikey = apikey['apiKey']
        self.api_key = apikey

    def get_token(self):
        self.get_api_key()
        # Base64 encode

        x_reference_id_bytes = str(self.x_reference_id) + ":" + str(self.api_key)
        key = x_reference_id_bytes.encode('ascii')
        authorization = base64.b64encode(key)
        Authorization = authorization.decode('ascii')

        # /token - POST
        # This operation is used to create an access token which can then be
        # used to authorize and authenticate towards the other end-points of the API.

        headers = {
            'Authorization': 'Basic {Authorization}'.format(Authorization=Authorization),
            'Ocp-Apim-Subscription-Key': 'bee5e0997ebd4f8c94946f647cf8f357',
        }
        token = requests.post('https://sandbox.momodeveloper.mtn.com/collection/token/', headers=headers)
        # token.status_code
        token = token.json()
        token = token['access_token']
        self.token = token

    def send_request(self, amount, external_id, party_id):
        self.get_token()
        apikey = self.api_key
        token = self.token
        # request topay - POST This operation is used to request a payment from a consumer (Payer). The payer will be
        # asked to authorize the payment. The transaction will be executed once the payer has authorized the payment.
        # The request to pay will be in status PENDING until the transaction is authorized or declined by the payer
        # or it is timed out by the system. Status of the transaction can be validated by using the GET
        # /requesttopay/<resourceId>
        print(token)
        url = "https://sandbox.momodeveloper.mtn.com/collection/v1_0/requesttopay"

        payload = '{ amount: ' + str(amount) + 'q' \
                                               ', currency: EUR, externalId: ' + str(
            external_id) + ', payer: { partyIdType: MSISDN, partyId: ' + str(
            party_id) + ' }, payerMessage: string, payeeNote: string }'

        headers = {
            'Authorization': 'Bearer {token}'.format(token=token),
            'X-Reference-Id': '{x_reference_id}'.format(x_reference_id=self.x_reference_id),
            'X-Target-Environment': 'sandbox',
            'Ocp-Apim-Subscription-Key': '{subscription_key}'.format(subscription_key=self.subscription_key),
            'Content-Type': 'application/json'
        }

        request_topay = requests.post(url, headers=headers, data=(payload))
        print(request_topay.status_code)

        return request_topay.status_code


